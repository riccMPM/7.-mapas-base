package pe.com.mapas.clase;

import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

public class MainActivity extends AppCompatActivity {

    private DrawerLayout drawerLayout;
    private NavigationView navigationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        drawerLayout = findViewById(R.id.drawerLayout);
        navigationView = findViewById(R.id.navigationView);

        setToolbar(); // Setear Toolbar como action bar
        selectItem(R.id.action_maps);
        setupBottomNavigation(); // Metodo que reacciona a los eventos del menu lateral

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                //Cuando apretamos el botón menu del Action Bar, abrimos menú lateral
                drawerLayout.openDrawer(GravityCompat.START);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {

        if (drawerLayout != null && drawerLayout.isDrawerOpen(GravityCompat.START))
            super.onBackPressed();
        else
            drawerLayout.openDrawer(GravityCompat.START); // Cuando apretamos el botón atrás, abrimos el menú lateral

    }

    private void setToolbar() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        final ActionBar ab = getSupportActionBar();
        if (ab != null) {
            // Poner ícono del drawer toggle
            ab.setHomeAsUpIndicator(R.drawable.ic_menu);
            ab.setDisplayHomeAsUpEnabled(true);
        }

    }

    private void setupBottomNavigation() {

        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                int id = item.getItemId();
                switch (id) {
                    case R.id.action_maps:
                        selectItem(item.getItemId());
                        return true;
                    case R.id.action_fragment1:
                        selectItem(item.getItemId());
                        return true;
                    case R.id.action_fragment2:
                        selectItem(item.getItemId());
                        return true;
                    default:
                        return true;
                }


            }
        });


    }

    private void selectItem(int idSelection) {
        drawerLayout.closeDrawers();

        Fragment fragment = getFragment(idSelection);

        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager
                .beginTransaction()
                .replace(R.id.flaContainer, fragment)
                .commit();

    }

    private Fragment getFragment(int idSelection) {

        switch (idSelection) {
            case R.id.action_maps:
                return MapasFragment.create();
            case R.id.action_fragment1:
                return UnoFragment.create();
            case R.id.action_fragment2:
                return DosFragment.create();
            default:
                return MapasFragment.create();


        }
    }
}
